﻿$packageName= 'xftp'
$toolsDir   = "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)"
$url        = 'https://gitlab.com/exileed/xshell/raw/master/xftp/6.0.0199/Xftp-6.0.0199r.exe?inline=false'

$packageArgs = @{
  packageName   = $packageName
  fileType      = 'exe'
  url           = $url
  silentArgs    = "-s"
}

Install-ChocolateyPackage @packageArgs