﻿$packageName= 'postman-canary'
$toolsDir   = "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)"
$url        = 'https://dl.pstmn.io/download/channel/canary/windows_32'
$url64      = 'https://dl.pstmn.io/download/channel/canary/windows_64'

$packageArgs = @{
  packageName   = $packageName
  fileType      = 'exe'
  url           = $url
  url64bit      = $url64
  silentArgs    = "-s"
}

Install-ChocolateyPackage @packageArgs