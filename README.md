# My chocolatey packages

You can find collection of [Chocolatey](https://chocolatey.org/) Windows [package manager](https://en.wikipedia.org/wiki/Package_manager) packages in this repository.

# List of packages

- [Postman Canary](https://getpostman.com/)
- [Xshell](https://www.netsarang.com/en/xshell/)
- [Xftp](https://www.netsarang.com/en/xftp/)
