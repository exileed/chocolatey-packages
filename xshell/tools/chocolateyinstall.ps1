﻿$packageName= 'xshell'
$toolsDir   = "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)"
$url        = 'https://gitlab.com/exileed/xshell/raw/master/xshell/6.0.0204/Xshell-6.0.0204r.exe?inline=false'

$packageArgs = @{
  packageName   = $packageName
  fileType      = 'exe'
  url           = $url
  silentArgs    = "-s"
}

Install-ChocolateyPackage @packageArgs